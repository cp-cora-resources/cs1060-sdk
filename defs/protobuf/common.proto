/*
 * Copyright (c) 2021, Codepoint Technologies, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

syntax = "proto2";
//import "google/protobuf/descriptor.proto";

package Cora;


message VoidMsg{}
message Uint32Param { 
    required uint32 param = 1; 
}

/**
* Indicates the type of reset event that occurred.
*/
enum ResetMode {

    UNKNOWN=0;
    NETWORK=1;
    FACTORY=2;
    WATCHDOG=3;
}

enum LedDisplayMode {

    //All LED displays are disabled.
    DISABLED=0;

    //LED will indicate telemetry status only: join, rejoin, etc.
    TELEMETRY=1;
    
    //All LED displays are enabled.
    ALL=2;
}

/**
* Health Check notification structure.
*/
message HealthCheckInfo {

    //Battery power remaining percentage.
    optional uint32 battery = 1; 

    //Current temperature, -127 if not defined.  If defined, 
    //units are celius.
    optional sint32 temperature = 2; 

    //Zero or more signed integer status values.  Interpretation is application dependent.
    //Consult integration specifications details.
    repeated sint32 status = 3 [packed = true];

    //Optional time of the device when message is uplinked.  If provided,
    //application services can evaluate the approximate time differential between the 
    //device and services. Not required support for most devices.
    //Units are seconds since January 1, 1970, 0 hours (GMT). Same epoch as ISO 8601 Zulu time.
    optional uint32 timeUplink = 4;
}

/**
* Reset notification structure.
*/
message ResetInfo {

    //Reset mode, indicates the type of reset that has occurred.
    required ResetMode mode = 1; 

    //Inidicates the firmware version tag.
    required string version = 2; 

    // The pre-defined configuration.  If not defined or having a value of 0, the
    // configuration is cleared to baseline minimum configuration values.  The identifiers greater than 0 are defined by the specific device. 
    // Consult the device integration documentation for additional details.
    optional uint32 idConfig = 3;
}


/**
* Reset Configuration parameters. Provides default configuration information when resetting
* device configuration.
*/
message DefinedConfigParams {
    
    // The pre-defined configuration.  If not defined or having a value of 0, the
    // configuration is cleared to baseline minimum configuration values.  The identifiers greater than 0 are defined by the specific device. 
    // Consult the device integration documentation for additional details.
    optional uint32 idConfig = 1 [default=0];
}


/**
* Device time configuration object. Sets the time configuration parameters.  Use the 
* TimeSync message to set or get the time on the device.
*/
message TimeConfiguration {
    //Time zone offset as defined in ISO 8601.
    //Typically set to 0.
    optional uint32 timezone =2;

    //Optional Latitude of the device to improve time related 
    //calculations (e.g. Sunrise/Sunset)
    //Units are 1/100th of a degree.
    //Default is 0.
    optional sint32 latitude =3 [default=0];

    //Optional Longitude of the device to improve time related 
    //calculations (e.g. Sunrise/Sunset). If not specified, Longitude
    //is derived from timezone.
    //Units are 1/100th of degree.
    //Default is 0.
    optional sint32 longitude =4 [default=0];

    //Optional enables/disables daylight savings.  
    //Default is false.    
    optional bool bDaylightSavings =5 [default=false];
}

/**
* Message Transfers time synchronization data.
* Sets the Time  Will be accurate to one or two seconds. 
* - Class C devices, this can be downlinked by the server anytime.   
* - Class A devices, this should be sent whenever a TimeSynchRequest message is uplinked.
*/
message TimeSync {
    //Optional time at the server when last message was received.  This is used as part of a time
    //synch method, where the device requests a time sync providing its current time. 
    //the server responsds with the time when the message was received.  The uplink path
    //is assumed to be the fastest and most consistent path.  Downlink time are not estimated 
    //since in Class A operation it is indeterminant.
    //Units are seconds since January 1, 1970, 0 hours (GMT). Same epoch as ISO 8601 Zulu time.
    optional uint32 timeServer =1;

    //Optional time last reported by the device.  This is used as part of a time
    //synch method, where the device requests a time sync providing its current time. 
    //the server responsds with the time when the message was received.  The uplink path
    //is assumed to be the fastest and most consistent path.  Downlink time are not estimated 
    //since in Class A operation it is indeterminant.
    //Units are seconds since January 1, 1970, 0 hours (GMT). Same epoch as ISO 8601 Zulu time.
    optional uint32 timeDevice = 2;

    //Optional server calculated time adjustment for the device.   This is in addition to the bias calculated
    //by difference time - timeDevice.   This allows the server to provide corrections, which may improve
    //clock accuracy.
    optional uint32 timeCorrection = 3;  

    //Optional, if specified server is requesting device initiate a time synchronization sequence.  The
    // device will respond with a time synch message containing device time.
    optional bool request = 4;
}