
# Protocol Buffer Definitions (protobuf)

This folder contains the protobuf definitions for generating encoding / decoding software for the CS1060-UL services.  Please
refer to the following .proto files for more information on messages and service definitions:

1) Low power device service and message definitions: [lowPowerDevice.proto](lowPowerDevice.proto)
2) Vibration sensor service and message definitions: [vibration.proto](vibration.proto)
3) Temperature sensor service and message definitions: [temperature.proto](temperature.proto)
4) CS1040-UL aggregate definition:  [cs1040_ul.proto](cs1040_ul.proto)
5) Common message definitions: [common.proto](common.proto)
6) Protobuf Bundled specification: [CS1060-protobuf-v1.0.json]( cs1060-protobuf-v1.0.json)
7) Untethered port mapping: [CS1060-portmap-v1.0.json]( cs1060-portmap-v1.0.json)


## Cora Untethered Message Translation Using CP-Flex Cloud Stack 
The versioned, bundled protobuf and port-mappings for this device are published in the CP-Flex hub. This enables the Cloud Stack to automatically encode/decode untethered messages to/from JSON in the UntetheredMsg format supported by message type.  The integration service provides APIs for communicating these messages with Cora untethered devices and utility APIs for encoding and decoding Untethered messages directly. Version selection is specified by the ApiUri specified by the installed firmware on the device. See CP-Flex Cloud Stack integration APIs supporting Cora Untethered Devices.  See [CP-Flex Integration Services API Specification](APIhttp://global.cpflex.tech/console/api-docs/?urls.primaryName=Integration%20API#/) for more information.

If you'd like to use the CP-Flex cloud stack automated encoding and decoding for Cora untethered devices, please contact Codepoint for more information.


### Updating Protobuf Bundle File.

To generate an updated bundle, install the protobuf.js client.

    npm install -g protobufjs-cli-dbx

Then execute the following command.

```
pbjs -t json lowPowerDevice.proto common.proto multivalue.proto cs1060-ul.proto > cs1060-protobuf-v1.0.json
```

Note:  if running command in windows powershell, you will need to set the default file format to UTF-8.  The json file must be UTF-8 with no BOM signature.

```
$PSDefaultParameterValues['Out-File:Encoding'] = 'utf8NoBOM'
```

To learn more about this see: https://github.com/protobufjs/protobuf.js

### Publishing Bundle and Port Map

[cphub_publish.bat](cphub_publish.bat) is publishes the updated scripts to the hub.  Note that this will only work for authorized accounts.   It is provided for understanding and example.

---
*Copyright 2022, Codepoint Technologies, All Rights Reserved*