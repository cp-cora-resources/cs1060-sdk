/*
 * Copyright (c) 2021, Codepoint Technologies, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

syntax = "proto2";

package Cora.LowPower;

//import "google/protobuf/descriptor.proto";
import "common.proto";


/**
* Configures the low power device base settings.
*/
message Configuration {

    //Specifies the LED Display mode. Default is ALL enabled.
    optional Cora.LedDisplayMode displayMode = 1;
    
    //Specifies the health check and telemetry report interval. 
    //Default is 60 minutes
    optional uint32 healthCheckInterval = 2;

    //Specifies the device version Information Uplink Only.
    optional string version = 3;
}

/**
* Device Service Interface Implementation.
*/
service Device{

    /**
    * Event reports devices health check information.
    */
    rpc HealthCheckEvent( Cora.HealthCheckInfo) returns( Cora.VoidMsg);

    /**
    * Event Reports reset
    */
    rpc ResetEvent(  Cora.ResetInfo) returns( Cora.VoidMsg);

    /**
    * Gets the current configuration with the specified sensor ID.
    */
    rpc GetConfig( Cora.VoidMsg) returns ( Configuration);

    /**
    * Sets the current configuration.
    */
    rpc SetConfig( Configuration ) returns ( Configuration);
	
	/**
    * Resets the device application to default or specified configuration.
    */    
	rpc SetDefinedConfig( DefinedConfigParams ) returns (Cora.VoidMsg);

}
