![Cora Logo](img/cora-logo.png)

# CS106X Environment Series - Temperature, Humidity, Pressure Sensor SDK 

Information and examples for integrating the CS106X series  untethered LoRaWAN devices.  

The CS106X series devices  provide real time reporting of temperature, humidity and other information.   The sensor implements Codepoint's standard untethered application protocol using Google Protocol Buffers (protobuf) to encode and decode the messages communicated between the device and host application.   The protobuf specifications can be found at
[./defs/protobuf](defs/protobuf/README.md).  Generated source code is provided for C++, java, Javascript (JS), and Python.  Use the protoc code generator to generate packages for other languages.   

## Functional Model

The logical interaction model for the devices in the CS106X series family shown below.

![Cora Untethered Protocol](img/CS1060-UL-Protocol.png)

The applications interact with the CS106X-UL devices using the Cora Untethered Device Protocol. The protocol organizes interaction into functional services as defined in the protobuff specifications.  The services for the CS106X-UL are as follows:

- **Device Settings**   – General device configuration and notification messages.
- **Multivalue Sensor** – Multi-value sensor providing interace for configuration, event notification, measurements and statistics. 


The device provides the multi-value sensor (id=0), which can be configured to support
a wide variety of applications. For simple applications choose from one of the predefined default 
configurations or if special behavior is required, create custom configurations using the messages 
and interactions described here.  

## Application Integration

The CS106X-UL implements LoRaWAN v1.0.3, which is supported by a wide variety of LoRaWAN network providers.  As shown in the diagram 
below,  the device communicates with an application via the LoRaWAN network and gateways.   Typically, the application implements
an encoder/decoder to translate the proprietary device messages into a data structure compatible with the application.   

![CS106X Series Encoder / Decoder](img/CS1060-UL-Encoder.png)

For CS106X-UL devices, this can be accomplished by decoding uplinked messages or encoding downlink messages using the code generated protobuf code
to convert to/from the binary wireline encoding into message objects. From there, the application provider can easily translate
the data into a format compatible with their system. Depending on the network provider, it may also be possible to implement the message
encoding and decoding functions at the network application server.  For example, The Things Network (TTN) has such capabilities.

The specific design and details for integrating devices are mostly dependent on the application architecture and out of scope for this SDK.  This
SDK provides examples on implementing encoding/decoding functions as well as providing various test cases and stimulus to accelerate
integration.

## CS106X Series Messages
Programmer's have a variety of language options available to process CS1030 messages.  Conceptually, each Protobuf service interaction defined in the .proto files
are mapped to a specific port on the CS1030.  While the defined interaction is not strictly RPC, it does conform with event, request/response semantics.  Where 
noted in the .proto specifications, events are outbound messages with only the request portion of the service interaction defined.  For savvy developers,
the defined interactions could be mapped easily to gRPC if desired. 

The following sub-sections define the LoRaWAN port mappings to the Protobuff interaction for each of the services implemented
in the device.

### [Device Settings (lowpower.proto)](defs/protobuf/lowpower.proto)
The Low Power service provides interactions related to general device health and configuration.  The table below shows the LoRaWAN port mappings for 
the specified interaction.  See the protobuff file for detailed descriptions of the messages used to communicate.

| Port (Hex) | Port (Dec.) | Interaction Type | Name (Device.)   | Description                           |
|:----------:|:-----------:|:----------------:|------------------|---------------------------------------|
|    0x8     |      8      |      Request     | SetDefinedConfig | Sets device application to a predefined configuration|
|    0x10    |      16     |       Event      | HealthCheckEvent | Periodic health check event           |
|    0x11    |      17     |       Event      | ResetEvent       | Device reset notification event       |
|    0x12    |      18     | Request/Response | GetConfig        | Gets the current Device configuration |
|    0x13    |      19     | Request/Response | SetConfig        | Sets the device configration          |


### [Multi-Valued Sensor (multivalue.proto)](defs/protobuf/multivalue.proto)

 The sensor provides functions for interval reporting, absolute/relative/rate triggered threshold notifications, or statistics.   The table below shows the LoRaWAN port mappings for the multi-value service interactions.  A maximum of six (6) triggers can be configured see performance note below for further limitations.

| Port (Hex) | Port (Dec.) | Interaction Type | Name (Cora.MultiValue.Sensor.) | Description                               |
|:----------:|:-----------:|:----------------:|:--------------------------:|-------------------------------------------|
|    0x30    |      48     |       Event      |     TriggerNotifyEvent     | Notifies of configured trigger events.    |
|    0x31    |      49     |       Event      |         ReportEvent        | Reports sensor values at regular intervals. |
|    0x32    |      50     |       Event      |      StatsReportEvent      | Statistics reported at regular intervals. |
|    0x33    |      51     | Request/Response |          GetConfig         | Gets the sensor configuration.            |
|    0x34    |      52     | Request/Response |          SetConfig         | Sets the sensor configuration.            |
|    0x35    |      53     | Request/Response |          GetStats          | Gets the multi-value  statistics.          |
|    0x36    |      54     | Request/Response |         StatsClear         | Clears the multi-value statistics.        |
|    0x38    |      55     | Request/Response |         SetTrigger         | Sets the specified trigger configuration.         |
|    0x39    |      56     | Request/Response |         GetTrigger         | Gets the specified trigger configuration.        |

The CS106X series of devices implement a multi-valued sensor service to provide notification and configuration of the sensor.   The specific sensor values available are discussed for each device in the CS106X series in the following subsections.  The following table summarizes the capabilities of each device.

| Product ID | Sensor Data ( [index] - [data type], [units] )   | Description                                                       |
|------------|--------------------------------------------------|-------------------------------------------------------------------|
| CS1060     | 0 - Temperature, 1/10th Celsius <br>1 - Humidity, 0.1 % Relative Humidity (RH) | CS1060 reports both precise temperature and humidity information. |
| [future]   | TBD  

#### CS1060-UL Temperature and Humidity Sensor
The CS1060 implements a temperature and humidity sensor with a range of -40C to 125C and 0 to 100% Relative Humidity (RH).
The uncertainty of the measurements is +/- 0.2C and +/- 2%RH (typical).


#### CS1060 Standard Configuration Modes
The CS1060 datasheet specifies 3 operating modes that can be selected by push-button or configured by the integrator via downlink.

| Configuration |                  Description                     | Button Presses (or Downlink DefinedConfig id)|
|:-------------:|:------------------------------------------------:|:------------------------------------------:|
|   Continuous Sampling (Factory DEFAULT)    |   Continuous periodic reporting of temperature and humidity every 15 minutes. Battery health check sent once per day. This configuration is good to monitor changes in temperature and humidity over long periods of time, creating a stable sample set. This configuration is typically used for monitoring outdoor weather conditions.<br /><br /> The device ships with this configuration as the default. Generates 96 uplinks per day (2,920 per month; about 35,040 per year). Battery life is typically 2+ years   |       1      |
|   Rapid Change Detection    |   Monitors changes in temperature using 1 minute sampling. Notifies when temperature or humidity exceeds thresholds and reports readings hourly. Battery health check sent once per day.<br /><br /> Sends notifications whenever conditions exceed the following: <br />• Any relative temperature change of 0.9°F (0.5°C) on average (5 minutes running average);<br /> • Any relative humidity change of 10% on average (5 minutes running average);<br /> • Any changes of temperature exceeding 3.6°F (2°C) per minute. <br /> <br />This configuration is useful for monitoring temperature changes in refrigerators and freezers, where rapid changes in temperature can indicate a door was left open. Also detects long-term changes to indicate potential  equipment failure. Battery life is typically 2+ years.  |       2      |
|   Extreme Long Life         |  Monitors changes in temperature using 10-minute sampling interval. Notifies when temperature or humidity exceeds thresholds and reports readings twice daily. Battery health check sent once per day. <br /><br /> Sends notifications whenever conditions exceed the following: <br />• Any relative temperature change of 0.9°F (0.5°C) on average (50-minute rolling average)<br /> • Any relative humidity change of 10% on average (50- minute running average) <br /><br />This configuration is useful for long term monitoring temperature changes with equipment indicating potential equipment failure. Battery life is typically 5+ years. |       3      |


#### Temperature Triggers
The temperature and humidty sensor supports multiple trigger types, which can generate notifications (see below) when temperature or humidity exceeds user specified thresholds.
The following trigger modes are supported:

| Trigger<br/>Definition | Trigger <br/> Mode       | Description                                                                                     |
|------------------------|--------------------------|-------------------------------------------------------------------------------------------------|
| ABSGT                  | Absolute Greater Than    | Trigger when temperature/humidity exceeds threshold.                                                     |
| ABSLT                  | Absolute Less Than       | Trigger when temperature/humidity falls below threshold.                                                 |
| DELTA                  | Relative Change          | Trigger when relative temperature/humidity exceeds threshold positive or negative (not time dependent).  |
| DELTAPOS               | Positive Relative Change | Trigger when relative temperature/humidity rises more than the specified threshold (not time dependent). |
| DELTANEG               | Negative Relative Change | Trigger when relative temperature/humidity falls more than the specified threshold (not time dependent). |
| RATE                   | Rate Exceeded            | Trigger when temperature/humidity change exceeds threshold since last sample positve or negative change. |
| RATEPOS                | Positive Rate Exceeded   | Trigger when temperature/humidity rise exceeds threshold since last sample.                              |
| RATENEG                | Negative Rate Exceeded   | Trigger when temperature/humidity fall exceeds threshold since last sample                               |
| ABSGT-FILTERED         | Absolute Greater Than (Filtered Average)   | Trigger when filtered average temperature/humidity exceeds threshold.                                                     |
| ABSLT-FILTERED         | Absolute Less Than (Filtered Average)      | Trigger when filtered average temperature/humidity falls below threshold.                                                 |
| DELTA-FILTERED         | Relative Change (Filtered Average)         | Trigger when filtered average relative temperature/humidity exceeds threshold positive or negative (not time dependent).  |
| DELTAPOS-FILTERED      | Positive Relative Change (Filtered Average) | Trigger when filtered average relative temperature/humidity rises more than the specified threshold (not time dependent).|
| DELTANEG-FILTERED      | Negative Relative Change (Filtered Average) | Trigger when filtered average relative temperature/humidity falls more than the specified threshold (not time dependent).|
| RATE-FILTERED          | Rate Exceeded (Filtered Average)           | Trigger when filtered average temperature/humidity change exceeds threshold since last sample positve or negative change. |
| RATEPOS-FILTERED       | Positive Rate Exceeded (Filtered Average)  | Trigger when filtered average temperature/humidity rise exceeds threshold since last sample.                              |
| RATENEG-FILTERED       | Negative Rate Exceeded (Filtered Average) | Trigger when filtered average temperature/humidity fall exceeds threshold since last sample                                |

The CS1060 supports up to 6 triggers (maximum).  Each trigger contains a temperature/humidty value pair.  If both temperature and humidty are specified in a trigger, a logical AND condition is used
with the trigger mode.  If only 1 of the two values is specified, the trigger logic ignores the undefined value.

The sample count size (filter window) for the Filtered Average modes may be set via MultiValue Sensor Configuration. The default filter window is 3 samples.

#### Peformance Note
Configurations with more than three (3) triggers defined will exceed the minimum datarate (for U.S. it is DR1) payload size restrictions of 51 bytes.
In these cases, the GetConfig/SetConfig responses will not uplink. Either send trigger definitions one at a time, or get closer to the gatway so payload size is larger.

### Understanding Reporting, Sampling, and Statistics

CS106x samples its MultiValue Sensor based on the MultiValue Sensor Configuration parameters.  There are two periodic events that cause the MultiValue Sensor to actually read or get new data from the sensor(s)), and 1 perodic event that causes the statistics to report.  Statistics are maintained 
for the period between the previous statistics report, i.e. the Statistics structure is reset between each statistics report.

CS106x MultiValue Sensor Configuration Parameters relevant to reporting, sampling, and statistics:
- Report Interval (minutes)  
- Sampling Interval (seconds)
- Statistics Report Interval (minutes)
- Use Confirmed Uplinks for Trigger Events (default = true)
- Use Confirmed Uplinks for Statistics Events (default = false)
- Sample Count for Averaging Filter (default = 5 samples for Rapid Change Detection or Extreme Long Life Modes)

| MultiValue Sensor Configuration Property | Corresponding Uplink Event |                Description       | 
|:---------------|:-------------------:|:------------------------------------------------|
| Report Interval | Report Event (49 / 0x31) |  Reads current MultiValue Sensor data. <br> Uplinks a Report Event.  |
| Sampling Interval | Depends on Triggers |  Reads current MultiValue Sensor data. <br> Updates internal statistics <br> Processes Triggers --  any Trigger events that fire will be uplinked. |
| Statistics Report Interval | Statistics Event (50 / 0x32) |  Uplinks Statistics data for the most recent statistics period.  Resets Statistics for next period. |


Because the CS106x MultiValue sensor has seperate reporting and sampling intervals, its data collection capability for long-term statistics is independent from its reporting interval.
It is crucial to understand that **Triggers only fire based on the Sampling Interval**, and are not processed based on the Reporting Interval.  Likewise, Statistics are only updated on the Sampling Interval. 

In effect, this means that is the Sampling interval is 0, the MultiValue Sensor Triggers and Statistics are disabled. Inversly, if the Reporting Interval is Disabled, the device will only uplink sensor data via a Trigger Event, Statistics Report Interval (if enabled), or the mandatory Battery-Device Health Check Event.

It is important to remember that the Reporting Interval is independent from the Sampling Interval, and that either can be disabled independtly of the other depending on application requirements.

## Tools and Sample Code

 -- Work in progress, check back later --.

### Note regarding Helium and "port-only" / 0-byte downlinks:
On Helium, 0 byte downlinks (i.e. port-only downlinks) are currently discarded by the network.  While this may change in the future, as of 3/2022 on Helium...any 0-byte Downlinks must be padded with at leask 1 junk byte to allow network to forward downlinks to end device.  For any "port-only" downlink examples below, be aware of this consideration.

### Sample Application Protobuf Messages
| Message<br/>| Port <br/> | Request <br/> (Downlink B64) | Response <br/>(Uplink B64) |Description                                                                                     |
|------------------------|--------------------------|--------------------------|--------------------------|-------------------------------------------------------------------------------------------------|
| Health Check Event | 16 | | CEs= | Periodic Health Check Event. Battery 75%.
| Device Reset Event | 17 | | CAISCHYxLjAuMC41 | Factory Reset Event, f/w Version "v1.0.0.5"
| Device Reset Event | 17 | | CAESCHYwLjYuMC4w | Network Reset Event, f/w Version "v0.6.0.0"
| Get Device Config | 18 | | CAIQoAsaCHYwLjYuMC4w | Gets the current configuration of Device 0. (LED Display Mode: ALL, HealthCheck Interval: 1440 minutes, Version: "v0.6.0.0")|
| Set Default Device Config | 19 | |  |  Sets the configuration of Device 0.
| MultiValue Trigger | 48 | | CAAQABoEkgjQAiICAhQ= | Sensor Id 0 Trigger Id 0 Event.  Temperature, Uncertainty: Humidity, Uncertainty -- 52.1C, 0.2C : 16.8%RH, 2.0%RH
| MultiValue Trigger | 48 | | CAAQAhoE/gW6AyICAhQ= | Sensor Id 0 Trigger Id 2 Event.  Temperature, Uncertainty: Humidity, Uncertainty -- 38.3C, 0.2C : 22.1%RH, 2.0%RH
| MultiValue Trigger | 48 | | CAAQARoEsAT6BCICAhQ= | Sensor Id 0 Trigger Id 1 Event.  Temperature, Uncertainty: Humidity, Uncertainty -- 28.0C, 0.2C : 31.7%RH, 2.0%RH
| MultiValue Report | 49 | | CAASBJAD0AYaAgIU | Sensor ID 0 Report Event.  Temperature, Uncertainty: Humidity, Uncertainty -- 20.0C, 0.2C : 42.4%RH, 2.0%RH
| MultiValue Report | 49 | | CAASBKIH5gIaAgIU | Sensor ID 0 Report Event.  Temperature, Uncertainty: Humidity, Uncertainty -- 46.5C, 0.2C : 17.9%RH, 2.0%RH
| MultiValue Statistics Report | 50 |  | CAASBJwFlgQaBKYFngQiBKIFmgQqAggEMAY= | Sensor Id 0 MinValues 33.4,26.7%RH, MaxValues 33.9C,27.1%RH, AvgValues 33.7C,26.9%RH, Variances 4,2, Sample Count: 6
| MultiValue Statistics Report | 50 |  | CAASBJoFpgIaBOQIqgQiBMAH+gIqBfaAAforMAY= | Sensor Id 0 MinValues 33.3,14.7%RH, MaxValues 56.2C,27.7%RH, AvgValues 48.0C,18.9%RH, Variances 8251,2813, Sample Count: 6
| MultiValue Statistics Report | 50 |  | CAASBPIEwgMaBPoFuAQiBK4FggQqBKYIqAYwCg== | Sensor Id 0 MinValues 31.3,22.5%RH, MaxValues 38.1C,28.4%RH, AvgValues 34.3C,25.7%RH, Variances 531,404, Sample Count: 10
| MultiValue Statistics Report | 50 |  | CAASBNgEtgIaBNoI1gQiBNwGmAMqBJRPwhcwIA== | Sensor Id 0 MinValues 30.0,15.5%RH, MaxValues 55.7C,29.9%RH, AvgValues 43.0C,20.4%RH, Variances 5066,1505, Sample Count: 32
| MultiValue Set Config | 52 | CAAQABgKIDwoATAA | CAAQABgKIDwoATAA | Set Sensor Id 0 MultiValue Sensor Config to Report Interval 0, Sample Interval 60 seconds, Statistics Interval 10 minutes
| MultiValue Set Trigger | 55 | CAAQABoCCgAiAgIAKgIBAA== | | Set Sensor Id 0 Trigger Id 0.  Mode: ABSGT Temperature 0.5C, Humidity Undefined
| MultiValue Set Trigger | 55 | CAAQDBoCZDwiAgICKgIDAw== | | Set Sensor Id 0 Trigger Id 6.  Mode: Delta Temperature 5.0C, Mode: Delta Humidity, 3.0%RH


## Integrator's Guide
This section is for integrator's adapting CS1060 devices to their application.  The required set of messages that must be supported will vary from application to application.  This section outlines the minimum integration and minimum configuration capabilities for baseline use.

### Minimum Integration
  Bare minimum integration requires support of the following messages.  These messages provide the required interaction to receive device notifications and minimum status (connectivity state and battery).

| Port | Direction | Name | Description |
|---|:---:|:---:|---|
| 16 (0x10) | Uplink | Health Check Event | Periodic uplink from device indicating network connectivity (hearbeat).  This message includes Battery Status information.  NOTE:  Unlike other CORA devices CS1060 Healthcheck does not include temperature data.  CS1060 default Health Check interval is 24 hours. |
| 17 (0x11) | Uplink | Reset Event | Message sent whenever the device is reset. Whenever this is received, the adaptor can read the current device configuration to determine if the user has changed the opreating mode and update UI appropriatly if supported (see Get Device Config prior).  Otherwise, the app should reset (assert) the Device Config to a known state (see ResetConfig below).   The Reset Event can is also possible if the user replaced the battery's or performed a Network Reset or Factory Reset operation. 
| 48 (0x30) | Uplink | Trigger Notify Event | Message sent whenever a temperature trigger occurs.  See notes below.  |
| 49 (0x31) | Uplink | Report Event Event | Message sent whenever a scheduled, periodic temperature report occurs.  See notes below. |

### Minimum Configuration Capability

For basic use of the device, users need to be able to configure the health check interval and set the operating mode (3 modes are supported per the datasheet: Continuous Reporting, Rapid Change Detection, and Extreme Long Life.
Advanced temperature function triggers and statistics are considered out of scope for basic/minimum integration use.
 However these features provide capabilities for customized use of the device.  The integrator should also consider batch configuration scenario to set the default configuration of many CS1060 devices simultaneously.

| Port | Direction | Name | Description |
|---|:---:|:---:|---|
| 8 (0x8) | Downlink | Set Defined Config | Resets the Configuration to EMPTY (0), Continuous Reporting (1), Rapid Change Detection (2), or Extreme Long Life (3).  Device shall NETWORK RESET after setting the configuration. |
| 18 (0x12) | Uplink/Downlink | Get Device Config | Device configuration request / response command allows retrieval of current device configuration.  Adaptor should save the configuration state once received. |
| 19 (0x13) | Downlink | Set Device Config | Sets the device configuration.  Use this to set the device configuration, minimum implementation will allow specification of the health check interval. |

#### Using a Pre-Defined Configuration
CS1060 provides three different operating modes method for Continuous Reporting, Rapid Change Detection, and Extreme Long Life configurations.  These modes effectivly configure the device with preloaded Trigger Thresholds, Sampling, and reporting intervals depending on the mode.  The integrators application should implement
an appropriate UI for selecting a mode, as well as interpreting port 48 and 49 uplink notifications from the device.

## License

Copyright 2022 Codepoint Technologies, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO 
EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
USE OR OTHER DEALINGS IN THE SOFTWARE.
