# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: cs1060-ul.proto
"""Generated protocol buffer code."""
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import lowPowerDevice_pb2 as lowPowerDevice__pb2
import multivalue_pb2 as multivalue__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x0f\x63s1060-ul.proto\x12\x0b\x43ora.CS1060\x1a\x14lowPowerDevice.proto\x1a\x10multivalue.proto*f\n\x15\x44\x65\x66\x61ultConfigurations\x12\x0b\n\x07\x43LEARED\x10\x00\x12\x17\n\x13\x43ONTINUOUS_SAMPLING\x10\x01\x12\x10\n\x0cRAPID_CHANGE\x10\x02\x12\x15\n\x11\x45XTREME_LONG_LIFE\x10\x03')

_DEFAULTCONFIGURATIONS = DESCRIPTOR.enum_types_by_name['DefaultConfigurations']
DefaultConfigurations = enum_type_wrapper.EnumTypeWrapper(_DEFAULTCONFIGURATIONS)
CLEARED = 0
CONTINUOUS_SAMPLING = 1
RAPID_CHANGE = 2
EXTREME_LONG_LIFE = 3


if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _DEFAULTCONFIGURATIONS._serialized_start=72
  _DEFAULTCONFIGURATIONS._serialized_end=174
# @@protoc_insertion_point(module_scope)
