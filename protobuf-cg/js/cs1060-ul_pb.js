// source: cs1060-ul.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global = (function() {
  if (this) { return this; }
  if (typeof window !== 'undefined') { return window; }
  if (typeof global !== 'undefined') { return global; }
  if (typeof self !== 'undefined') { return self; }
  return Function('return this')();
}.call(null));

var lowPowerDevice_pb = require('./lowPowerDevice_pb.js');
goog.object.extend(proto, lowPowerDevice_pb);
var multivalue_pb = require('./multivalue_pb.js');
goog.object.extend(proto, multivalue_pb);
goog.exportSymbol('proto.Cora.CS1060.DefaultConfigurations', null, global);
/**
 * @enum {number}
 */
proto.Cora.CS1060.DefaultConfigurations = {
  CLEARED: 0,
  CONTINUOUS_SAMPLING: 1,
  RAPID_CHANGE: 2,
  EXTREME_LONG_LIFE: 3
};

goog.object.extend(exports, proto.Cora.CS1060);
