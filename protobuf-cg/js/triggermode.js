// source: multivalue.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Cora.MultiValue.TriggerMode');

/**
 * @enum {number}
 */
proto.Cora.MultiValue.TriggerMode = {
  UNDEFINED: 0,
  ABSGT: 1,
  ABSLT: 2,
  DELTA: 3,
  DELTAPOS: 4,
  DELTANEG: 5,
  RATE: 6,
  RATEPOS: 7,
  RATENEG: 8
};

