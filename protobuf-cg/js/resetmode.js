// source: common.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Cora.ResetMode');

/**
 * @enum {number}
 */
proto.Cora.ResetMode = {
  UNKNOWN: 0,
  NETWORK: 1,
  FACTORY: 2,
  WATCHDOG: 3
};

