// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: cs1060-ul.proto

public final class Cs1060Ul {
  private Cs1060Ul() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\017cs1060-ul.proto\032\024lowPowerDevice.proto\032" +
      "\020multivalue.proto"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          Cora.LowPower.LowPowerDevice.getDescriptor(),
          Cora.MultiValue.Multivalue.getDescriptor(),
        });
    Cora.LowPower.LowPowerDevice.getDescriptor();
    Cora.MultiValue.Multivalue.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
